import React from 'react';
import { Link } from 'react-router-dom';

const Menu = () => (
    <>
        <Link to="/">Home</Link>&nbsp;|&nbsp;
        <Link to="/contact-list">Contact List</Link>&nbsp;|&nbsp;
        <Link to="/new-contact">New Contact</Link>
    </>
);

export default Menu;