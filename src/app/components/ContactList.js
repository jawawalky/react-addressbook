import React from 'react';
import ContactListItem from './ContactListItem';

export default function ContactList (props) {
    const items =
        props.contacts.map(
            contact => <ContactListItem {...props} contact={contact} actions={props.actions} />
        );
    return (
        <>
            <h2>Contacts</h2>
            <ul>{items}</ul>
        </>
    );
}
