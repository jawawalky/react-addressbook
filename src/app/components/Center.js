import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Menu from './Menu';
import Content from './Content';

const Center = () => (
    <BrowserRouter>
        <Menu />
        <hr />
        <Content />
    </BrowserRouter>
);

export default Center;
