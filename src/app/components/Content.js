import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ContactList from './ContactList';
import ContactForm from './ContactForm';
import Home from './Home';
import { Contact } from '../model/Contact';

export default class Content extends React.Component {

    state = {
        contacts: [
            new Contact(1, 'Duke', 'Sun Boulevard 1', '94016', 'San Francisco'),
            new Contact(2, 'Sue', 'Eclipse Street 13', '12024', 'New York')
        ],
        contact: new Contact(0, '', '', '', '')
    };

    addContact = (contact) => {
        const contacts = this.state.contacts;
        contacts.push(contact);
        this.setState({
            contacts: contacts,
            contact: new Contact(0, '', '', '', '')
        });
    }

    deleteContact = (id) => {
        const contacts =
            this.state.contacts.filter(contact => contact.id !== id);
        this.setState({ contacts: contacts });
    }

    actions = {
        add:    this.addContact,
        delete: this.deleteContact
    };

    render() {
        return (
            <>
                <Switch>
                    <Route
                        path   = '/contact-list'
                        render = {props => <ContactList {...props} contacts={this.state.contacts} actions={this.actions} />} />
                    <Route
                        path   = '/new-contact'
                        render = {props => <ContactForm {...props} contact={this.state.contact} actions={this.actions} />} />
                    <Route path='/'>
                        <Home />
                    </Route>
                </Switch>
            </>
        );
    }

};
