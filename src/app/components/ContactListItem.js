import React from 'react';

export default class ContactListItem extends React.Component {
    
    delete = id => (() => this.props.actions.delete(id));

    render() {
        const { contact } = this.props;
        return (
            <li key={contact.id}>
                <span>{contact.info()}</span>|
                <button onClick={this.delete(contact.id)}>Delete</button>
            </li>
        );
    }
}
