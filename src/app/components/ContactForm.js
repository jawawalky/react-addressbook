import React from 'react';
import { Contact } from '../model/Contact';

export default class ContactForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id:      props.contact.id,
            name:    props.contact.name,
            street:  props.contact.street,
            zipCode: props.contact.zipCode,
            city:    props.contact.city
        };
    }

    newContact = () => {
        return new Contact(
            this.state.id,
            this.state.name,
            this.state.street,
            this.state.zipCode,
            this.state.city
        );
    }

    submit = (event) => {
        event.preventDefault();
        this.props.actions.add(this.newContact());
        this.props.history.push('/contact-list')
    }

    handleChange = (event) => {
        const target = event.target;
        const field  = target.name;
        const value  = target.value;
        this.setState({[field]: value});
    }

    render() {
        const inputField = (name, value) => {
            return (
                <div>
                    <label>{name}</label>
                    <input
                        name={name}
                        type='text'
                        value={value}
                        onChange={this.handleChange} />
                </div>
            );
        }
        
        return (
            <>
                <h2>New Contact</h2>
                <form onSubmit={this.submit}>
                    {inputField('id',      this.state.id)}
                    {inputField('name',    this.state.name)}
                    {inputField('street',  this.state.street)}
                    {inputField('zipCode', this.state.zipCode)}
                    {inputField('city',    this.state.city)}
                    <input type='submit' value='Submit' />
                </form>
            </>
        );    
    }

}
