export class Contact {

    constructor(id, name, street, zipCode, city) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.zipCode = zipCode; 
        this.city = city;
    }

    info() {
        return `[${this.id}:${this.name}]: ${this.street}, ${this.zipCode} ${this.city}`;
    }

}
