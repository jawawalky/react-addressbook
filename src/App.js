import React from 'react';
import Header from './app/components/Header'
import Center from './app/components/Center';
import Footer from './app/components/Footer'
import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <Center />
      <Footer />
    </div>
  );
}

export default App;
